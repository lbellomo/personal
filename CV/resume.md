Lucas Bellomo
============

-------------------                                                       ----------------------------
Website: [lbellomo.pages.dev](https://lbellomo.pages.dev/)                         Email: [lbellomo@gmail.com](mailto:lbellomo@gmail.com)
Github: [\@lbellomo](https://github.com/lbellomo)                                  Argentina (w/ Italian citizenship)
Stack Overflow: [\@Lucas](https://stackoverflow.com/users/3954379/lucas)                                         
-------------------                                                       ----------------------------

----

>  Self-taught pythonist with 7+ years of experience and some studies in physics. \
>  Learning Rust and Elixir. Free Software/Open Culture Enthusiast. \
>  Organizer of tech events such as FLISoL, Django Girls, TEDx.

----

Experience
----------
2023-present
:   **[Gestorando](https://gestorando.com/)**, Software engineer/SRE, Remote

Re-writing scrapers from selenium to requests, running at a fraction of the time and cost. \
Adding system-wide monitoring and alerts. Added Ansible to automatically provision machines. Profiling and optimizing code. \
Tools: Python - Prometheus - Ansible - AWS - PostgreSQL

2021-2023
:   **[WURU](https://wuru.site/)**, Software engineer, Remote

Maintaining and evolving an ETL pipeline for patient flow in clinics and hospitals. \
Backend development for the webapp in fastapi and helping the data science team with the infra. \
Tools: Python - Airflow - AWS - Redshift - ECS - FastApi - Scientific Stack (numpy, pandas, etc) - Streamlit

2020-2021
:   **[Zarego](https://www.zarego.com/)**, Principal Engineer, Remote

I migrated a large serverless application (+90 lambda functions) to AWS SAM (infra-as-code), 
I took care of the CI/CD and I finished the development with python. Also training junior and non-python programmers. \
Tools: Python - AWS SAM/Cloudformation, Lambda, Dynamodb, ApiGateway

2019-2020
:   **[Ascentio Technologies](https://www.ascentio.com.ar/en/home/)**, Python Dev

Ground segment of the SABIA-MAR mission. I worked on 3 systems and migrate code to py3. \
Tools: Python - Docker - BDD 

Technical Experience
--------------------

Programming Languages
:   **Python:** 
Experience with modern python,  good knowledge of the Standard Library,
 the scientific stack (numpy/scipy, matplotlib, pandas, polar, scikit-learn and networkx),
 backend libraries like fastapi and flask and web-scrapping. PEP08 advocate.

:   Good knowledge of **Git**, **Shell Script**, **Gnu+Linux**, **AWS**

:   Intermediate knowledge of **Docker**, **Podman**, **Ansible**, **Prometheus**

:   Basic knowlodge (I learned but never used it professionally) of **Terraform**, **Nomad**, **k3s**

:   Learning of **Rust**, **Elixir** and **self-hosting**.


Education
---------

2012-2018
:   **Student of Physics at Faculty of Exact, Physical-Chemical and Natural Sciences**; National University of Río Cuarto

2016
:   **Workshop inScientific programming techniques (WTPC16)**; University of Buenos Aires

2015
:   **XVII Giambiagi Winter School**; University of Buenos Aires

2014
:   **Argentinian School of computing for scientific applications GPGPU**; Balseiro Institute (Bariloche)

Languages
---------
* Human Languages:

     * English (Intermediate)
     * Spanish (native speaker)

Archivements
------------

* [MetaData, predicción de clicks](https://metadata.fundacionsadosky.org.ar/competition/1/) (ML comtetition) - Winner, 1st place - 2018, organized by Fundación Sadosky

* [Nasa Space App](https://2016.spaceappschallenge.org/challenges/solar-system/asteroid-mining/projects/piratas-de-cascotes-2) - 1st Best Mission Concept - "Piratas de Cascotes" Team - 2016, Buenos Aires

* Hackaton Agro Datos - 3rd place - "Germineitor" Team - 2015, FaMAF, Córdoba
